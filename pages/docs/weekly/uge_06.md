---
Week: 06
Content: Introduktion til faget og introduktion til Linux.
Material: 
Initials: MESN
# hide:
#  - footer
---

# Uge 06 - *Introduktion til faget*

## Emner

Ugens emner er:

- Introduktion til faget System sikkerhed
- Opsætning af ubuntu server

## Mål for ugen

I denne uge er målet at de studerende forstår hvad de forventes af dem
i faget, og hvad de overordnet læringsmål for faget er.

### Praktiske mål

- Alle studerende har forståelse for fagets formål.
- Alle studerende kan (Nogenlunde)tolke læringsmålene for faget.
- Alle studerende har en fungerende Ubuntu Server VM. 

### Læringsmål der arbejdes med i faget denne uge
_I denne uge arbejde vi ikke med konkrette læringsmål fra studieordningen, men forståelsen for dem_

## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen |
| 08:45  | Gruppe arbejde med forståelse for fagets læringsmål (Opgave 1) |
| 09:45  | Pause |
| 10:00  | Opgave 1 forsat - Udfyld padlet |
| 10:10  | Opsamling på læringsmål i plenum |
| 10:30  | Opsætning af ubuntu server VM |
| 11:20  | Status på opsætning af VM og opsamling på dagen |
| 11:30  | Lektion slut |
