---
Week: 09
Content:  Linux bruger system & adgangskontrol
Material: xxx
Initials: MESN
# hide:
#  - footer
---

# Uge 09 - *Linux bruger system & adgangskontrol*

## Emner

Ugens emner er:

- Bruger systemer og rettigheder i Linux

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende kan oprette, slette eller ændre en bruger i bash shell
- Den studerende kan ændre bruger rettighederne i bash shell
- Den studerende har en grundlæggende forståelse for principle of privilege of least
- den studerende har en grundlæggende forståelse for mandatory og discretionary access control.

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:** 
- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- OS roller ift. sikkerhedsovervejelser
- **Færdigheder:**
- Udnytte modforanstaltninger til sikring af systemer
- **Kompetencer:**
- Håndtere enheder på command line-niveau
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre it-sikkerhedsmæssige hændelser


## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen |
| 08:25 | Opsamling på Mitre ATT&CK øvelse |
| 08:35 | Oplæg: Bruger konto & princippet om compartmentalization(isolering) |
| 08:45 | Individuel Opgave: Bruger kontoer i Linux |
| 09:20 | Opsamling på opgaver med bruger kontoer i Linux |
| 09:30 | Oplæg: Principle of privilege of least samt MAC & DAC |
| 09:45 | Pause |
| 10:00 | Individuel Opgave: Bruger rettigheder i Linux |
| 10:40 | Opsamling på Bruger rettigheder i Linux |
| 10:50 | Oplæg: Opbevaring af bruger konto oplysninger samt passwords|
| 11:20 | Opsamling på dagen |
| 11:30 | Eftermiddags opgave: Bruger konto filer og passwords, opsætning af ubuntu server på proxmox + afslut rest fra øvelserne|
