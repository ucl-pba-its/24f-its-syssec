---
Week: 16
Initials: MESN
# hide:
#  - footer
---

# Uge 16 - *Overvågning med SIEM systemer*

## Emner

Ugens emner er:

- Monitorering med Wazuh.
- Wazuh regler og dekoder.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Alle studerende har konfigureret Auditd og wazuh til at detekter eksekvering af ondsindet kommandoer.
- Alle studerende har implementeret en dekoder i Wazuh
- Alle studerende har implementeret en regel i Wazuh

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:** ..
- **Færdigheder:**
  - Kan implementerer systematisk logning og monitering af enheder
  - Kan analyser logs for hændelser og følge et revision spor
- **Kompetencer:**
  - Kan håndtere enheder på command line-niveau
  - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at
    forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.
  - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen samt gennemgang af opgave 36 |
| 08:25 | Wazuh opgave 36,38 + eftermiddags opgaver |
| 11:30 | Lektion slut|