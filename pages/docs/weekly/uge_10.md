---
Week: 10
Content: Fysisk sikkerhed og Host-based firewall
Initials: MESN
# hide:
#  - footer
---

# Uge 10 - *Fysisk sikkerhed og Host-based firewall*

## Emner

Ugens emner er:

- Fysisk sikkerhed
- Host-based firewall.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- En forståelse for at hændelser i den fysiske verden også kan udgører en it-sikkerheds trussel.
- At den studerende kan lave en grundlæggende opsætning af en firewall.
- At den studerende har forståelse for firewall'ens rolle på en host.

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
  - Generelle sikkerhedprocedurer.
  - Relevante  it-trusler
- **Færdigheder:** ..
  - Udnytte modforanstaltninger til sikring af systemer
- **Kompetencer:** ..
  - Håndtere værktøjer til at fjerne forskellige typer af endpoint trusler.
  - Håndtere udvælgelse og anvend af praktiske til at forhindre it-sikkerhedsmæssige hændelser. 
## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen |
| 08:30 | Oplæg samt gruppe øvelser: Fysisk sikkerhed |
| 09:30 | Oplæg: Firewalls |
| 09:45 | Pause |
| 10:00 | Individuelle Øvelser: Iptables firewall |
| 11:20 | Opsamling på dagen |
| 11:30 | Eftermiddags øvelser Opsætning af Wazuh på Proxmox Ubuntu server|