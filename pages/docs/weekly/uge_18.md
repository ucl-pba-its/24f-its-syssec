---
Week: 18
Initials: MESN
# hide:
#  - footer
---

# Uge 18 - *CVE,CWE og opdateringer*

## Emner

Ugens emner er:

- CVE
- CWE
- Opdatering

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- At hver studerende kan forklar hvad en CVE er.
- At hver studerende kan forklar hvad en CWE er.
- At den studerende forstår anti virus applikations samhæng med overvåg
- At hver studerende har lavet en opsætning med clamav og maldet
- At hver studerende har lavet en skanning med RKhunter

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
  - Relevante it-trusler  
- **Kompetencer:**
  - Håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler.

## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen |
| 08:30 | Oplæg: Konceptuel gennemgang af HTTPS | 
| 09:15 | Pause: 10 minutter | 
| 09:25 | Oplæg: malware| 
| 09:45 | Pause | 
| 10:00 | Gruppe øvelse med malware | 
| 10:20 | Opsamling på gruppe øvelse | 
| 10:30 | Praksis opgaver: Opsætning og detektering af malware | 
| 11:30 | Pause | 
| 12:15 | Oplæg CVE | 
| 12:30 | Gruppe øvelse med CVE | 
| 12:55 | Opsamling på gruppe øvelse | 
| 13:05 | Oplæg: opdateringer | 
| 13:15 | Oplæg: CWE | 
| 13:25 | Gruppe øvelse: CWE |
| 13:45 | Pause |
| 14:00 | Opsamling: gruppe øvelse |
| 14:10 | Arbejde med rest opgaver, server opsætning på Proxmox osv| 
| 15:30 | Dagen slut | 
