# Opgave - Gruppe og projekt opgaver

## Information
Formålet med gruppe opgaverne idag, er en kort genopfriskning af grundlæggende projekt arbejde i grupper.
Opgaver som disse kan ofte virke banale, men udbyttet viser sig tilgengæld ofte at være ret stort for de
enkelte grupper. Så tag opgaverne alvorligt.


**Alle CL strukturer der anvendes, kan findes i it's learning planen for idag, under resourcer**

## Opgaver

### Opgave 1 - afstem projekt mål
_Tids estimate: 10 minutter_  
_I denne opgave skal Strukturen Ordet rundt anvendes_  
Hvert gruppemedlem skal på skift forklar hvad de overordnet mål for gruppens semester projekt er.

### Opgave 2 - SMART modellen
_Tids estimate: 15 minutter_  
Hvert gruppen skal individuelt læse om (SMART modellen)[https://emu.dk/grundskole/ledelse/strategisk-ledelse/smart-modellen-tydelige-maal-alle]

### Opgave 3 - Benyt SMART modellen
_Tids estimate: 10 minutter_  
Hvert gruppemedlem skal på skift forklar hvordan de mener at SMART modellen kan anvendes til i semester projektet.

### Opgave 4 - Formalisering af projekt målene.
_Tids estimate: 20 minutter_  
Gruppen skal nu i fælleskab formaliserer Projekt målene ved at skrive dem ned.
_Husk at målene skal skrives i et klart, specifikt og entydiget sprog (SM)_

### Opgave 5 - Afstem milepælene i semester projektet.
_Tids estimate: 15 minutter_  
Gruppen skal nu afstemme alle milepælene i projektet. Altså hvilken dele af projektet 
skal være afsluttet hvornår.

### Opgave 6 - Læringsmål
_Tids estimate: 10 minutter_  
I denne opgave skal gruppen genbesøge læringsmålene fra studieordningen. og identificer
de læringsmål som semester projektet dækker.

## Links
[SMART modellen](https://emu.dk/grundskole/ledelse/strategisk-ledelse/smart-modellen-tydelige-maal-alle)