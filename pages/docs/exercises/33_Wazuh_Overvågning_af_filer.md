# Opgave 33 - Overvågning af filer i directory med Wazuh

## Information
Wazuh agenten kan overvåge ændringer i en file eller et helt directory. I denne opgave skal
der opsættes overvågning på et directory ift. til ændringer i denne.

Når man opsætter overvågning i Wazuh, skal der oftest ændres i wazuh agentens konfiguration file `/var/ossec/etc/ossec.conf`.
Strukturen af denne konfiguration file er af ældre dato og er i formatet XML. Inden man ændre i filen kan det godt betale sig og
orientere sig om filens generelle opbygning ved at gå på opdagelse i den.

Når der i denne og de kommende opgaver benævnes _blokken_ menes der inden for de omklammerende tags 
Eksempelvis er _hej_ skrevet inde i _syscheck_ blokken: `<syscheck>hej</syscheck>`

Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten
Equivalent til denne øvelse, kan findes i wazuh dokumentationen [her](https://documentation.wazuh.com/current/user-manual/capabilities/file-integrity/use-cases/reporting-file-changes.html).
  
**En forudsætning for denne og alle kommende øvelser, er at øvelserne eksekveres på en host med en aktiv Wazuh agent**
  
## Instruktioner  


### Opsætning af Wazuh agent
_Alle kommandoer eksekveres på den overvåget host_  
1. opret directoriet `/home/SecretFolder`  
2. Åben Wazuh agentens konfiguration file i en tekst editor. Filen findes i `/var/ossec/etc/ossec.conf`  
3. I blokken `<syscheck>` skal følgende block tilføjes `<directories check_all="yes" report_changes="yes" realtime="yes">/home/SecretFolder</directories>`   
4. genstart wazuh agenten med kommandoen `systemctl restart wazuh-agent`  
5. opret filen `/home/SecretFolder/secretFile.txt`.  
6. Tilføj teksten `Bad mojo` til filen `/home/SecretFolder/secretFile.txt`  
7. Slet filen `/home/SecretFolder/secretFile.txt`  
  
### Wazuh Dashboard
1. I Wazuh dashboards, går ind på _security events_  
![Security events](./Images/Wazuh_agent/Wazuh_dashboard_security_events.jpg)
2.  I søge felte skal der indtastes `rule.id:(550 OR 553 OR 554)`  
![Search security events](./Images/Wazuh_Dashboard/Search_Security_events.jpg)
3. Dette bør frembring 3 nye begivenheder i security alerts  
![Security alerts](./Images/Wazuh_Dashboard/File_Integrity_Security_alerts.jpg)


## Links
[File integrity monitoring with Wazuh](https://documentation.wazuh.com/current/proof-of-concept-guide/poc-file-integrity-monitoring.html)  
[Reglesæt i Wazuh](https://documentation.wazuh.com/current/user-manual/ruleset/index.html)  
