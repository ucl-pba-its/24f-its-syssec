# Opgave 23 - Tillad indgående trafik fra specifikke ICMP-beskeder.

## Information
ICMP-pakker bruges til såkaldte "ping requests" og anvendes ofte af angribere i rekognosceringsfasen, hvor netværket bliver skannet. Man kan skjule sin vært ved at blokere ICMP-pakker. At blokere ICMP-pakker er ikke i sig selv en foranstaltning mod et angreb, men det besværliggør dog angriberens arbejde en smule i rekoniserings fasen. Nogle enkelte typer af ICMP-pakker er dog belejlige at kunne modtage.

I opgaven skal 3 typer af ICMP-pakker tillades.

## Instruktioner
1. Tillad ICMP-pakker af type 3 med kommandoen `sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 3 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT`.
2. Tillad ICMP-pakker af type 11 med kommandoen `sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 11 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT`.
3. Tillad ICMP-pakker af type 12 med kommandoen `sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 12 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT`.
4. Udskriv regelkæden. Kan du finde fejlen? og hvordan kan den rettes?
5. De ICMP-pakker, der er blevet tilladt, er hver især af en bestemt type. Undersøg hvad hver af de 3 typer betyder.

## Links  
[ICMP Types](https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml)
