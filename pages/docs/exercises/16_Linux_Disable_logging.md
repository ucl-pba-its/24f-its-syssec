# Opgave 16 - Nedlukning af logservice

## Information
Loggingdaemons såsom Rsyslog kan lukkes ned på lige fod med andre applikationsprocesser i Linux.
Formålet med denne øvelse er at demonstrere, at der kan slukkes for loggingen på den enkelte vært.

En modstander kan udnytte dette, når denne har opnået privilegeret adgang. Dette vil blokere for senere
efterforskning af hændelser på værten.

## Instruktioner
1. Eksekver kommandoen `service rsyslog stop`. Efter dette vil der ikke længere blive genereret logs i operativsystemet.
2. Eksekver kommandoen `service rsyslog start`.

Typisk er det kun superbrugere, der kan lukke ned for en logservice. Det betyder, at hvis en angriber kan lukke ned for logging-servicen, kan han også lukke ned for eventuelle sikkerhedsmekanismer, der er i samme operativsystem. Overvej, hvordan man kan undgå dette, f.eks. ved logovervågning via netværk.

## Links