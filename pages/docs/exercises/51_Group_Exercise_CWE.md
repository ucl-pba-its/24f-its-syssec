# Opgave 51 - (Gruppeopgave) Undersøgelse af CWE'er

## Information
Common weakness enumeration(CWE) database, indeholder lister over typiske svagheder der kan forekomme i IT systemer.
En CWE er ikke nødvendigvis et udtryk for en sårbarhed, men i stedet hvilken svagheder der kan forårsage sårbarheder.

I denne øvelse skal gruppe sammen undersøge en række CWE'er og udlede en kort forklaring på hver CWE.


## Instruktioner
I jeres gruppe skal i sammen undersøge betydningen af følgende svagheder:
  
- CWE-20
- CWE-653
- CWE-287
- CWE-272
- CWE-1329
- CWE-306

I skal lave en kort konceptuel forklaring på hver CWE.


# Links
[What is CWE](https://cwe.mitre.org/about/index.html)
[Mitre CWE database](https://cwe.mitre.org/)