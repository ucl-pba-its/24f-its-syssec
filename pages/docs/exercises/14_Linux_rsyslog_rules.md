# Opgave 14 -  Logging regler for rsyslog kan ændres 
  
## Information
Reglerne for, hvad rsyslog skal logge, findes i filen 50-default.conf.

Reglerne er skrevet som facility.priority action.

Facility er programmet, der logger. For eksempel: mail eller kern.
Priority fortæller systemet, hvilken type besked der skal logges. Beskedtyper identificerer, hvilken prioritet den enkelte log har. Nedenfor er beskedtyper listet i prioriteret rækkefølge, højeste prioritet først:

emerg.
alert.
crit.
err.
warning.
notice.
info.
debug.
Således er logbeskeder af typen emerg vigtige beskeder, der bør reageres på med det samme, hvorimod debugbeskeder er mindre vigtige. Priority kan udskiftes med wildcard *, hvilket betyder, at alle beskedtyper skal sendes til den fil, der er defineret i action.

Formålet er følgende øvelse er at introducere opsætningen som Rsyslog anvender til
at skrive log linjer fra specifikke applikationer og processer, til specifikke log filer.

## Instruktioner

1. i ryslog directory, skal filen `50-default.conf` findes.
2. Åben filen `50-default.conf`
3. Dan et overblik over alle de log filer som der bliver sendt beskeder til.
4. Noter hvilken filer mail applikationen sender log beskeder til, ved priorteringen _info_ , _warn_ og _err_

## Links

  
