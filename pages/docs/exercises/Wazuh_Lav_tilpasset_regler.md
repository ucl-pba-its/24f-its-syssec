# Opgave 37 - Wazuh sårbarheds skanning

## Information

## Instruktioner
Test log: 

### overvåg file
`/var/log/audit/audit.log`
`type=USER_AUTH msg=audit(1682344104.419:477): pid=8094 uid=0 auid=4294967295 ses=4294967295 subj=unconfined msg='op=PAM:authentication grantors=? acct="attacker" exe="/usr/sbin/sshd" hostname=192.168.230.7 addr=192.168.230.7 terminal=ssh res=failed'UID="root" AUID="unset"`
Regex: ^type=USER_AUTH\s|(res=failed)
Trig på:
- terminal=ssh
- type=USER_AUTH
- res=failed
Fields:
acct=<Brugernavn>
hostname=<Hostname>
addr=<IP ADRESSE>
pid=<PROCESS ID>


### Wazuh log test
`/var/ossec/bin/wazuh-logtest`
### Wazuh manager

#### Forældre decoder
`nano /var/ossec/etc/decoders/local_decoder.xml`
```xml
<decoder name="medium">
  <prematch>^type=USER_AUTH\s|(res=failed)</prematch>
</decoder>
```
Restart Wazuh-manager service
Kør Wazuh logtest
#### Child decoder
`nano /var/ossec/etc/decoders/local_decoder.xml`
Hent felter
```xml
<decoder name="medium_child">
  <parent>medium</parent>
  <regex offset="after_parent">^\s(\.+) is an awesome company, check them out at (https://\.+)</regex>
  <order>company,website</order>
</decoder>
```
Restart Wazuh-manager service
Kør Wazuh logtest
#### Regel
`nano /var/ossec/etc/rules/local_rules.xml`
```xml
<group name="medium,socfortress">
 <rule id="100021" level="5">
    <decoded_as>medium</decoded_as>
    <field name="company">SOCFortress</field>
    <description>Go check out $(company) at $(website)!</description>
  </rule>
  </group>
```
Restart Wazuh-manager service
Kør Wazuh logtest
#### Regel
### Wazuh Agent

#### Overvåg log file

## Links
