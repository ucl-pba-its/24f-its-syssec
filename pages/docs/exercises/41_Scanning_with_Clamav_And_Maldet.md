# Opgave 41 - skanning med Clamav og Maldet

## Information
I denne opgave skal der arbejdes med malware skanninger.
Maldet eksekver skanningerne automatisk, så det der skal gøres, er
at der downloades nogle malware simulerings filer ned i  `/home` directory
som er overvåget(opgave 40). Efter lidt tid bør Maldet og Clamav detekter filerne
og sætte dem i karanten, hvilket fremgår ved at filerne ikke længere er i `/home`
directoriet. Herudover bør loggen vise at der blev fundet nogle malware filer.

Filerne som bruges til malware simulering, er ikke funktionelt  malware, de deler blot
signature, altså de er ikke skadelige.

## Instruktioner
1. Hent malware simulerings filer ned i directoret `/home`
```
sudo wget https://secure.eicar.org/eicar.com
sudo wget https://secure.eicar.org/eicar.com.txt
sudo wget https://secure.eicar.org/eicar_com.zip
sudo wget https://secure.eicar.org/eicarcom2.zip
```
2. vent 10 sekunder, og bekræft i loggen `/usr/local/maldetect/logs/event_log` at filerne er blevet detekteret.  
![malware detected](./Images/malware/Malware_detected.jpg)
3. bekræft at alle 4 filer ikke længere er i `/home` directory.
4. Reflekter over hvordan man kan overvåge for malware med Wazuh? Kan en tidligere tilgang bruges?


## Links
[Malware simulerings filer](https://www.eicar.org/download-anti-malware-testfile )
[Maldet](https://github.com/rfxn/linux-malware-detect)