# Opgave 22 - Tilad indagående trafik for loopback

## Information
loopback interfacet (lo,også kendt som 127.0.0.1 eller localhost) bruges til en række ting internt i
Linux. Derfor kan det give mening at tillade indadgående trafik fra denne. 
 
## Instruktioner
1. tillad forbindelser fra loopback interfacet med kommandoen `sudo iptables -I INPUT 1 -i lo -j ACCEPT`

Udover at Linux bruger loopback til interne operationer, så er input fra loopback også en stor hjælp ift. 
evt. fejlfinding på applikationer. 

## Links

  
