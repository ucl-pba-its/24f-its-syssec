# Opgave 40 - Konfigurer maldet

## Information
I denne opgave skal Maldet konfigureres, ved at ændre i filen `/usr/local/maldetect/conf.maldet` 
Først skal stien til filen som indeholder listen med alle overvåget directories defineres. Og
herefter skal email alarmen sættes op. Her benyttes Linux bruger konto mailen. Og til sidst
skal der tændes for automatisk karatane for malware filer.


## Instruktioner
1. åben Maldet konfigurations filen, med kommandoen `sudo nano /usr/local/maldetect/conf.maldet`
2. Find linjerne som vist nedenunder:  
![Maldet monitor path default](./Images/malware/Maldet_montior_path_default.jpg)  
3. Ret linjerne til:  
![Maldet path to monitor](./Images/malware/Maldet_montior_path_monitor_path_folder.jpg)  
1. I toppen af filen, ændre _email_alert_ og  _email_addr_ til de værdier som er vist på billede nedenunder. _Dog udskift warzuh-user med dit brugernavn_
![Email alerts](./Images/malware/Email_alerts.jpg)  
1. Ændre værdien  af  _quarantine_hits_ til 1, som vist på billede nedenunder.
![Quarantine hits](./Images/malware/quarantine_hits.jpg)  
1. Opret filen `/usr/local/maldetect/monitor_paths`. Der er i denne file de overvåget  directories defineres.
2. Vi overvåger blot et enkelt directory, Tilføj linjen `/home` til filen.
3. start maldet daemon med kommandoen `systemctl start maldet`

## Links
[Maldet](https://github.com/rfxn/linux-malware-detect)