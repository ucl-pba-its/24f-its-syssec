Her er din tekst med nogle korrektioner:

---

# Opgave 15 - Logrotation i Linux

## Information
Når der løbende bliver indsamlet logs, vil logfiler typisk kunne blive meget store over tid og optage meget plads på lagermediet. For at undgå dette kan man rotere logs i et givet tidsrum. For eksempel kan en log roteres hver 3. uge. Hvis logfilen `auth.log` roteres, vil den skifte navn til `auth.log.1`, og en ny fil ved navn `auth.log` vil blive oprettet og modtage alle nye logs. `auth.log.1` bliver en såkaldt _backlog_-fil, som kan flyttes til arkiv hvor filen kan arkiveres (Eller filen kan slettes hvis _rentention_ tiden er overskredet).

Formålet med denne øvelse er at demonstrer hvordan grundlæggende log rotation fungerer med Rsyslog.

## Instruktioner
I disse opgaver skal der arbejdes med logrotation i Linux (Ubuntu).

Du kan finde hjælp i [Ubuntu man page](https://manpages.ubuntu.com/manpages/xenial/man8/logrotate.8.html) til logrotate.

1. Åben filen `/etc/logrotate.conf`.
2. Sæt logrotationen til daglig rotation.
3. Sæt antallet af backlog-filer til 8 uger.

Du kan prøve og selv eksperimenter med log rotationen. Hvad er den lavest mulige værdi.
Kan man lave rotation ud fra fra filens størrelse? 



## Links