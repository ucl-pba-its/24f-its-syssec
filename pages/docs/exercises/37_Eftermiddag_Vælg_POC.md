# Uge 16 - Eftermiddags opgave - Vælg en POC til detektering

## Information

Som nævnt i de fortløbende øvelser, har Wazuh dokumentationen nogle _Proof of concept_ guides. Gruppen skal udvælge to af disse
guides, og implementerer dem. Det skal naturligvis ikke være nogen af dem, som svare til de tidligere øvelser.


## Instruktioner
1. Udvælg to detekteringer som skal implementeres fra [proof of concept guide](https://documentation.wazuh.com/current/proof-of-concept-guide/index.html)
2. Implementer detekteringerne.

## Links
[Proof of concept guide](https://documentation.wazuh.com/current/proof-of-concept-guide/index.html)
