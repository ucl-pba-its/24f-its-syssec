# Opgave 50 - (Gruppeopgave) Undersøgelse af CVE'er

## Information
Kendte sårbarheder bliver publiceret i en såkaldt _CVE database_, F.eks. [Mitre CVE](https://cve.mitre.org/).
Det er disse databaser som de fleste software sårbarheds skanner anvender, til at lede efter kendte sårbarheder.
  
I denne øvelse skal gruppen manuelt Undersøge flere forskellige kendte sårbarheder.
Der findes flere forskellige CVE database som der kan søge i, Nedstående er der
3 eksempler på database der kan tilgåes via en hjemmeside:

- [Mitre CVE](https://cve.mitre.org/)
- [CVE details](https://www.cvedetails.com/)
- [Tenable CVE search](https://www.tenable.com/cve) _Anvendt af sikkerheds værktøjet tenable._


## Instruktioner
I jeres gruppe skal i sammen undersøge betydningen af følgende sårbarheder:
  
- CVE-2023-32269 
- CVE-2023-31436
- CVE-2014-0160
- CVE-2022-47509
- CVE-2021-44228
- CVE-2022-26903

I skal finde den konceptuelle forklaring på hver sårbarhed Samt deres CVVS score.
