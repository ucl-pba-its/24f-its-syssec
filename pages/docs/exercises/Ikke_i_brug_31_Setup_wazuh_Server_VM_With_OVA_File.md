# Opgave 31 - Opsætning af Wazuh server VM med OVA File.

## Information

  
## Instruktioner

### Opsætning af VM
1. Hent Wazuh [OVA](https://packages.wazuh.com/4.x/vm/wazuh-4.4.1.ova)
2. Åben den downloaded OVA file i WMWare
![Åben OVA file](./Images/OpenOVAInWMWare.jpg)
3. Navngive den importeret VM og specificer storage path  
![Navngiv importeret VM](./Images/NamingOVAImportedVM.jpg)  
  
Vær opmærksom på at Wazuh WM som standard er opsat med følgende specifikationer:
![Wawzuh VM specs](./Images/Wazuh_Wm_Specs.jpg)  
Bemærk at Hukommelsen er sat til 8 GB og netværks adapteren er bridgede.
8 GB hukommelse er at foretrække. Men fungerer også med 2GB, man skal dog væbne sig med tålmodighed da systemet bliver en del langsommere.  

Netværket på VM'en kan med fordel sættes til NAT.  
![VM netværks opsætning](./Images/Wazuh_VM_Network_Conntection.jpg)  
VMWare's Netværk opsætning bør se således ud:
![VMWare netværks opsætning](./Images/VMWare_Network_configuration.jpg)

### Yderlige forberedelse af Wazuh WM
1. Start den importeret VM. Her mødes du af en forspørgelse på credentials.  
![Wazuh credential screen](./Images/Wazuh_OVA_Not_Secure_By_Default.jpg)  
Læg mærke til at brugernavn og password bliver skrevet på skærmen. Herudover er der brugt  et default kodeord. Default kodeord o.l. er dårlig praksis, særligt i software som håndter sikkerhed.
1. Log ind med credentials (Bemærk at tastatur indstillingerne er engelsk, så bindestregen er ikke hvor den plejer at være)
2. Sæt tastatur layout til dansk med kommandoen `localectl set-keymap dk`
3. Noter VM'ens ip adresse med kommandoen `ip a`
_Det er ip 4 adressen fra interfacet eth0 som skal bruges_
1. log ud med kommandoen `logout` (Ikke slukke)

### Afprøv Wazuh
1. I en browser på din host(lokal host) indtast  url'en https:// Wazuh VM IP
2. Autentificer dig selv med brugernavnet _admin_ og kodeordet _admin_ (Igen default password)


## Links
[Wazuh VM](https://documentation.wazuh.com/current/deployment-options/virtual-machine/virtual-machine.html)
