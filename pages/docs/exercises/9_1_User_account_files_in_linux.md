# Opgave 9.1 - Bruger konto filer i Linux.

## Information
På Linux systemer er alle operativ systemets lokale brugerkontoer opbevaret i filer. Det 
skal du kigge nærmer på i disse opgaver.

**Husk og noter alle jeres besvarelser jeres cheatsheet**

## Instruktioner

### Filer tilknyttet bruger systemet - opbevaring af bruger kontoer
I linux er der som udgangspunkt to filer som er intressant ift. bruger systemet.
Den ene er filen passwd. I de fleste distributioner kan denne findes i stien `/etc/passwd`.
passwd indeholder alle informationer om bruger kontoer(undtaget passwords).

I denne opgave skal du udforske passwd filen.

1. Se rettighederne for passwd med kommandoen `ls <filenavn> -al`.
2. Overvej om rettighederne som udgangspunkt ser hensigtmæssige ud? og hvorfor der er de rettigheder som der er.
3. Udskriv filens indhold _Hvis du har glemt hvordan så kig i dit cheat sheet. øvelsen blev udført i opgave 7_
4. Samhold filens indhold med nedstående format (BEMÆRK: ikke alle kolonner er nødvendigvis tilstede)  
`Brugernavn:Password(legacy, ikke brugt længere derfor x istedet):Bruger id:Gruppe id:Bruger information:Hjemme directory:Default shell`

### Filer tilknyttet bruger systemet - passwords
Filen `shadow` opbevare passwords til alle bruger kontoer.
Alle passwords i `shadow` er hashes, som sikring mod udvekommende adgang til bruger kontoer.

I opgaven skal du udforske filen `shadow`.

1. udskriver rettighederne for filen `shadow`
2. Overvej rettighederne i samhold med `Privilege of least` princippet.
3. Udskriv filens indhold.
4. Samhold filens indhold med nedstående format.  
`:brugernavn:password(hashet):Sidste ændring af password:Minimum dage ind password skift:Maksimum dage ind password skift:Varslings tid for udløbet password:Antal dage med udløbet password inden dekativering af konto:konto udløbs dato`  
  
Nogen af jer er endnu ikke introduceret til password hashes, hvilket er okay. I skal blot noter
i jeres cheatsheet at i kan finde en forklaring på hashes i shadow filen i denne opgave.

Følgende viser et eksempel på et hash i shadow filen. 
`$y$j9T$GfMsEAQ8t9EkjiOiDzVRA0$cWqq3SrEZED3EvJYMFD/G1TYn8lgWOaSM8IvjCeD4j2:19417`  
formattet er som følger:  
`$hash algoritme id$salt$has$`  
Hash algoritmernes id kan man slå op. I eksemplet er `y` brugt. Det vil sige at hash algoritmen til at 
generer hashen er `yescrypt`.  

## Links
- [Et eksempel på hvordan man kan lave krav til kodeord kompleksiten i Linux](https://www.xmodulo.com/set-password-policy-linux.html )
- [Grundlæggende bruger styring i Linux](https://www.youtube.com/watch?v=19WOD84JFxA )
- [useradd - opret en ny bruger konto](https://linux.die.net/man/8/useradd )
- [userdel - Slet en bruger konto](https://linux.die.net/man/8/userdel )
- [usermod - ændring af en eksisterende bruger konto](https://linux.die.net/man/8/usermod )
- [passwd - ændring af brugers password (blandt andet)](https://man7.org/linux/man-pages/man1/passwd.1.html )
- [su - skift til anden bruger konto](https://man7.org/linux/man-pages/man1/su.1.html )
  
  
