# Opgave 13 - rsyslog introduktion

Her er teksten med nogle korrektioner:

---

## Information
De fleste Linux-distributioner i dag har to daemons (applikationsprocesser), som logger parallelt med hinanden. Der er rsyslogd og journald. Den praktiske forskel mellem de to logdaemons er, at rsyslogd logger i plain text filer, hvorimod journald logger i binære filer.

I faget systemsikkerhed arbejdes der primært med rsyslog (i Ubuntu Linux).
  
Formålet med følgende øvelser er at introducerer Rsyslog konfigurations filerne.

## Instruktioner

### Opsætning af `locate` til søgning.
Kommandoen `find` er god til søgning af filer, men `locate` kan også med fordel anvendes.

1. Installer locate med kommandoen `sudo apt install locate`.
2. Opdater "Files on disk" databasen med `sudo updatedb`.

### Dan overblik over Rsyslog logfilerne på operativsystemet.

1. Brug `locate` til at finde alle filer med ordet `rsyslog`.
2. Dan dig et generelt overblik over filerne. Er der mange tilknyttede filer?

### Rsyslog konfigurationsfilen.
Rsyslog konfigurationsfilen indeholder den generelle opsætning af rsyslog daemon, herunder hvem der ejer logfilerne, og hvilken gruppe der er tilknyttet logfilerne. Herudover har den modulopsætning. Moduler er ekstra funktionaliteter, som man kan tilføje til rsyslog.

1. Brug locate til at finde rsyslog filen `rsyslog.conf`.
2. Åbn filen med `nano`.
3. I konfigurationsfilen, find afsnittet "Set the default permissions for all log files".
4. Noter, hvem der er filens ejer, og hvilken gruppe logfilerne er tilknyttet.
5. Udforsk de andre områder af filen.

## Links

  
