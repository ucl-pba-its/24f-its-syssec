# Opgave 39 - Installer Clamav og Maldet

## Information
I denne opgave skal arbejde med anti virus softwaren Linux Malware detect(Maldet) og Clamav.
Maldet er et open source anti virus projekt med automatisk opdatering af virus signature (statisk analyse).
Clamav er også et open source anti virus projekt, dog specialiseret til skanning af e-mail filer, hvilket
betyder at den har  et hurtig file søgnings motor.

Maldet understøtter brugen af ekstern søge motorer til file skaning. Derfor skal vi bruge Clamav og 
Maldet sammen. Maldet bliver den primærer anti virus applikation, og clamAV kommer til at fungerer som
søge motor til skaning af filer på operativ systemet.


## Instruktioner
1. Installer Clamav samt værktøjerne `wget` og `inotify-tools` med kommandoen `sudo apt install clamav wget inotify-tools`
2. I de følgende trin skal vi manuelt installer Maldet. Ved manuel installering er det den bruger som  installer applikationen som ejer den. For at undgå at skulle skifte ejerskab på alle filerne efter installationen kan du med fordel skifte til `root` brugeren nu, med kommandoen  `sudo su -`.
3. Hent Maldet med kommandoen `wget https://www.rfxn.com/downloads/maldetect-current.tar.gz`
4. udpak Maldet med kommandoen `tar xzvf maldetect-current.tar.gz`
5. skift sti til det udpakket Maldet `cd maldetect-1.6.5`
6. Eksekver installations scriptet med kommandoen`./install.sh`
7. Maldet er afhængning af tekst editoren `ed`, installer den med følgende kommando `sudo apt install ed`
8. Installer Clamav daemon med kommandoen `sudo apt install clamav-daemon -y`.

## Links  
[Maldet](https://github.com/rfxn/linux-malware-detect)  
[Inotify-toosl](https://github.com/inotify-tools/inotify-tools)  
[Clamav-daemon](https://www.clamav.net )
