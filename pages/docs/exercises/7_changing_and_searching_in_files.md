# Opgave 7 - Ændring og søgning i filer

## Information
Formålet med denne opgave er at introducere de grundlæggende Linux-kommandoer.

I opgaven skal der eksperimenteres med Linux CLI-kommandoer i BASH shell.
Fremgangsmåden er, at du bliver bedt om at eksekvere en kommando, og herefter notere hvad der sker. Målet med disse øvelser er, at du skal opbygge et _Cheat sheet_ med Linux-kommandoer i dit GitLab repo og få en generel rutinering med grundlæggende Linux Bash-kommandoer. Det betyder følgende for alle trin i opgaven:

1. Udfør kommandoen.
2. Observer resultatet, og noter det herefter i dit _Cheat sheet_. _Altså efter hver eksekveret kommando, skal du kunne redegøre for, hvad den gjorde._

## Links til beskrivelse af kommandoerne i opgaven:  
[touch](https://man7.org/linux/man-pages/man1/touch.1.html)  
[cp](https://man7.org/linux/man-pages/man1/cp.1.html)  
[mkdir](https://man7.org/linux/man-pages/man1/mkdir.1.html)  
[mv](https://man7.org/linux/man-pages/man1/mv.1.html)  
[rm](https://man7.org/linux/man-pages/man1/rm.1.html)  
[echo](https://man7.org/linux/man-pages/man1/echo.1.html)  
[cat](https://man7.org/linux/man-pages/man1/cat.1.html)  
[grep](https://man7.org/linux/man-pages/man1/grep.1.html)  
  
## Instruktioner
**Husk at notere i dit cheatsheet efter hvert trin!**
**En del af opgaven er at træne informations søgning ift. Linux, så lad vær med at bruge ChatGPT**

1. I `Home directory`, eksekver kommandoen `touch minfile.txt`
2. I `Home directory`, eksekver kommandoen `cp minfile.txt kopiafminfile.txt`
3. I `Home directory`, eksekver kommandoen `mkdir minfiledir`
4. I `Home directory`, eksekver kommandoen, `mv minfile.txt minfiledir`
5. I `Home directory`, eksekver kommandoen, `rm -r minfiledir`

_I de næste trin skal der arbejdes med oprettelse af filer med tekst indhold, her bliver redirect operatoren introduceret (>). Operatoren tager outputet fra kommandoen på venstre side og skriver til filen på højre side._

6. I `Home directory`, eksekver kommandoen `echo "hej verden" > hejverdenfil.txt`
7. I `Home directory`, eksekver kommandoen `cat hejverdenfil.txt`
8. I `Home directory`, eksekver kommandoen `echo "hej ny verden" > hejverdenfil.txt`
9. I `Home directory`, eksekver kommandoen `cat hejverdenfil.txt`
10. I `Home directory`, eksekver kommandoen `echo "hej endnu en ny verden" >> hejverdenfil.txt`
11. I `Home directory`, eksekver kommandoen `cat hejverdenfil.txt`

_I de næste trin introduceres pipe operatoren (|). Den tager outputtet fra kommandoen på venstre side, og giver det videre til kommandoen på højre side_  
  
12. I `/etc/`, eksekver kommandoen `cat adduser.conf`. 
13. I `/etc/`, eksekver kommandoen `cat adduser.conf | grep 1000`. 
14. I `/etc/`, eksekver kommandoen `cat adduser.conf | grep no`. 
15. I `/`, eksekver kommandoen `grep no /etc/adduser.conf`
16. I `/`, eksekver kommandoen `ls -al | grep proc`
17. I `/etc/`, eksekver kommandoen `ls -al | grep shadow`
   
## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)
 
